/*
 *
 *  MIDI Module Test/Example Code
 *  Created by Michael Morrow 4/27/2020
 *
 *  The Laser Harp project uses the VS1053 chip to produce sounds based on
 *  MIDI data being streamed from the Arduino
 *
 */

#include "SimpleMidi.h"

#define VS1053_RX  2
#define VS1053_RES 9
#define VOLUME 127
#define REVERB 0

#define OCTAVE 1
#define STRING 0

#define EIGHTH 500
#define REST  4000

uint8_t notes[2][4] = {{0, 4, 7, 0},
                       {2, 2, 2, 3}};

int i;

void setup(){
  Serial.begin(9600);
  Serial.println("VS1053 MIDI test");
  
  pinMode(LED_BUILTIN, OUTPUT);
  triple_blink(LED_BUILTIN);

  midiInit(VS1053_RX, VS1053_RES);

  // midiSetInstrument(INST_ACO_GUITAR_NYLON);
  midiSetInstrument(INST_ACO_GUITAR_STEEL);  
  // midiSetInstrument(INST_ELECTRIC_BASS);
  // midiSetInstrument(INST_ELECTRIC_PIANO1);
  // midiSetInstrument(INST_SQUARE_LEAD);
  // midiSetInstrument(INST_PIZZICATO_STRINGS);
  // midiSetInstrument(INST_APPLAUSE);
  midiSetVolume(127);
}

void loop(){
  for(i = 0; i < 4; i++){
    midiNoteOn(notes[STRING][i], notes[OCTAVE][i]);
    delay(EIGHTH);
    midiNoteOff(notes[STRING][i], notes[OCTAVE][i]);
  }
  for(i = 2; i >= 0; i--){
    midiNoteOn(notes[STRING][i], notes[OCTAVE][i]);
    delay(EIGHTH);
    midiNoteOff(notes[STRING][i], notes[OCTAVE][i]);
  }
  
  delay(2500);
  triple_blink(LED_BUILTIN);
}

void triple_blink(uint8_t pin){
  digitalWrite(pin, HIGH);
  delay(250);
  digitalWrite(pin, LOW);
  delay(250);
  digitalWrite(pin, HIGH);
  delay(250);
  digitalWrite(pin, LOW);
  delay(250);
  digitalWrite(pin, HIGH);
  delay(250);
  digitalWrite(pin, LOW);
  delay(250);
}
