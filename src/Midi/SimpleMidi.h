/*
 *
 *  A Very Simple & Streamlined libarary for interfacing with the vs1053b through MIDI
 *  Written by Michael Morrow for Light Luthier's Laser Harp project 4/27/20
 *
 */

#ifndef SMIDI_H
#define SMIDI_H

#include "Arduino.h"
#include <SoftwareSerial.h>

//all MIDI status bytes are preceded by MIDI_
#define MIDI_NOTE_OFF 0x80
#define MIDI_NOTE_ON  0x90
#define MIDI_PROGRAM_CHANGE 0xC0
#define MIDI_CONTROL_CHANGE 0xB0
#define MIDI_BANK_SELECT   0x00
#define MIDI_CHANNEL_LEVEL 0x07
#define MIDI_EFFECT_LEVEL  0x5B

//almost all vs1053 specific data bytes preceded by VS1053_
#define VS1053_BANK_DRUMS1  0x78
#define VS1053_BANK_DRUMS2  0x7F
#define VS1053_BANK_MELODIC 0x79

//all instruments preceded by INST_
#define INST_ELECTRIC_PIANO1 5
#define INST_ELECTRIC_PIANO2 6
#define INST_ACO_GUITAR_NYLON 25
#define INST_ACO_GUITAR_STEEL 26
#define INST_ELECTRIC_BASS 34
#define INST_CELLO 43
#define INST_GOBLINS 102
#define INST_OCARINA 80
#define INST_TROMBONE 58
#define INST_PIZZICATO_STRINGS 46
#define INST_ORCH_HARP   47
#define INST_SQUARE_LEAD 81
#define INST_APPLAUSE   127

SoftwareSerial* midi_stream = NULL;

void midiInit(uint8_t txPin, uint8_t resPin){
  midi_stream = new SoftwareSerial(0, txPin);
  midi_stream->begin(31250);

  digitalWrite(resPin, LOW);
  delay(10);
  digitalWrite(resPin, HIGH);
  delay(10);

  midi_stream->write(MIDI_CONTROL_CHANGE);
  midi_stream->write((uint8_t)MIDI_BANK_SELECT);
  midi_stream->write(VS1053_BANK_MELODIC);
}

void midiSetInstrument(uint8_t inst) {
  if (midi_stream == NULL) return;
  // page 32 has instruments starting with 1 not 0 :(
  if (--inst > 127) return;
  
  midi_stream->write(MIDI_PROGRAM_CHANGE);  
  midi_stream->write(inst);
}


void midiSetVolume( uint8_t vol) {
  if (midi_stream == NULL) return;
  if (vol > 127) return;
  
  midi_stream->write(MIDI_CONTROL_CHANGE);
  midi_stream->write(MIDI_CHANNEL_LEVEL);
  midi_stream->write(vol);
}

void midiSetReverbLevel(uint8_t level){
  if (midi_stream == NULL) return;
  if (level > 127) return;
  
  midi_stream->write(MIDI_CONTROL_CHANGE);
  midi_stream->write(MIDI_EFFECT_LEVEL);
  midi_stream->write(level);
}

void midiNoteOn(uint8_t string, uint8_t octave) {
  if (midi_stream == NULL) return;
  if (string > 11) return;
  if (octave > 5) return;

  uint8_t note = string + octave * 12 + 36;
  
  midi_stream->write(MIDI_NOTE_ON);
  midi_stream->write(note);
  midi_stream->write(127);
}

void midiNoteOff(uint8_t string, uint8_t octave) {
  if (midi_stream == NULL) return;
  if (string > 11) return;
  if (octave > 5) return;

  uint8_t note = string + octave * 12 + 36;
  
  midi_stream->write(MIDI_NOTE_OFF);
  midi_stream->write(note);
  midi_stream->write(127);
}

#endif