/*
 *
 *  Sonar.h - Library created for interfacing with the HC-SR04 Sonar Sensor module
 *  Created by Michael Morrow for Light Luthier's Laser Harp Project, 4/2/20
 *
 *  Add support for multiple HC-SR04's at one time
 *
 */

#include "Arduino.h"
#include "Sonar.h"

Sonar::Sonar(int echo, int trigger){
	pinMode(echo, INPUT);
	_echo = echo;
	pinMode(trigger, OUTPUT);
	_trig = trigger;
  digitalWrite(_trig, LOW);
}

int Sonar::_duration(){
  digitalWrite(_trig, HIGH);
  delayMicroseconds(10);
  digitalWrite(_trig, LOW);
  return pulseIn(_echo, HIGH);
}

int Sonar::distanceIn(){
  return _duration()/74/2;
}

int Sonar::distanceCm(){
  return _duration()/29/2;
}