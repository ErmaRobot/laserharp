/*
 *
 *  Sonar.h - Library created for interfacing with the HC-SR04 Sonar Sensor module
 *  Created by Michael Morrow for Light Luthier's Laser Harp Project, 4/2/20
 *
 *  Add support for multiple HC-SR04's at one time
 *
 */

#ifndef Sonar_h
#define Sonar_h

#include "Arduino.h"

class Sonar{
  public:
    Sonar(int echo, int trigger);
    int distanceIn();
    int distanceCm();
  private:
  	uint8_t _echo;
  	uint8_t _trig;
  	int _duration();
};

#endif