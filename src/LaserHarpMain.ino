// defines pins numbers
const int stepPin = 3; 
const int dirPin = 4; 
const int laserPin = 10;
 
void setup() {
  
  // Sets the two pins as Outputs
  pinMode(stepPin,OUTPUT); 
  pinMode(dirPin,OUTPUT);
  pinMode(laserPin, OUTPUT);
}
void loop() {
  
  digitalWrite(dirPin,HIGH); // Enables the motor to move in a particular direction
  
  for(int x = 0; x < 100; x++) {

    if(x == 35 || x == 40 || x == 45 || x == 50 || x == 55 || x == 60 || x == 65 || x == 70)
    {
      digitalWrite(laserPin, HIGH);
      delay(1);
      digitalWrite(laserPin, LOW);
    }
    digitalWrite(stepPin,HIGH); 
    delayMicroseconds(700); 
    digitalWrite(stepPin,LOW); 
    delayMicroseconds(700); 
  }

  
  
  digitalWrite(dirPin,LOW); //Changes the rotations direction
  // Makes 400 pulses for making two full cycle rotation
  for(int x = 0; x < 100; x++) {
    if(x == 35 || x == 40 || x == 45 || x == 50 || x == 55 || x == 60 || x == 65 || x == 70)
    {
      digitalWrite(laserPin, HIGH);
      delay(1);
      digitalWrite(laserPin, LOW);
    }
    digitalWrite(stepPin,HIGH);
    delayMicroseconds(700);
    digitalWrite(stepPin,LOW);
    delayMicroseconds(700);
  }

}
